# Copyright 2018 Wproject - Aitzol Berasategi - https://aitzol.eus
#
# This file is part of PyCalculator Ver.1.2.
#
# This project is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3.
#
# PyCalculator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or ANY OTHER COMMERCIAL PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from tkinter import *

root = Tk()
root.resizable(False,False)
root.title("Calculator")
mainFrame=Frame(root)
mainFrame.pack()

##--- screen --- row 1##

screenContent = StringVar()
screenContent.set("0")
screen=Entry(mainFrame, text=screenContent)
screen.grid(row=1, column=1, padx=10, pady=10, columnspan=4)
screen.config(background="#000000", fg="#00ff00", justify="right")

##--- functions ---##

def pushNum(n):
    num = n
    currentContent = screenContent.get()
    try:
        if(operations.opReq):
            screenContent.set("")
            operations.opReq = False
    except:
        operations.opReq = False

    if(currentContent[-1] == "0") and (len(currentContent)==1) and (num != "."):
        screenContent.set(num)
    elif(currentContent.find('.')!=-1) and (num == "."):
        if(operations.operation != ""):
            screenContent.set("0.")
        else:
            screenContent.set(currentContent)
            print("Point already entered!")
    elif (num == "."):
        screenContent.set(currentContent + num)
    else:
        screenContent.set(screenContent.get() + num)

    #screenContent.get()

def delete():

    currentContent = screenContent.get()
    if(len(currentContent)>1):
        screenContent.set(currentContent[:-1])
    else:
        screenContent.set("0")

def clear():
    screenContent.set("0")
    print("CLEAR")

class math_operations():
    op = []
    operation = ""

    def __init__(self):
        self.result = 0

    def sum(self):
        try:
            if self.operation!="sum":
                self.res(self.operation)
            else:
                self.res("sum")

        except:
            print("No operators selected yet.")

        finally:
            self.screenCapture()
            self.op.append(self.currentContent)
            print(self.op)
            self.opReq = True
            self.operation = "sum"

    def sub(self):
        try:
            if self.operation!="sub":
                self.res(self.operation)
            else:
                self.res("sub")

        except:
            print("No operators selected yet.")

        finally:
            self.screenCapture()
            self.op.append(self.currentContent)
            print(self.op)
            self.opReq = True
            self.operation = "sub"

    def mul(self):
        try:
            if self.operation!="mul":
                self.res(self.operation)
            else:
                self.res("mul")

        except:
            print("No operators selected yet.")

        finally:
            self.screenCapture()
            self.op.append(self.currentContent)
            print(self.op)
            self.opReq = True
            self.operation = "mul"

    def div(self):
        try:
            if self.operation!="div":
                self.res(self.operation)
            else:
                self.res("div")

        except:
            print("No operators selected yet.")

        finally:
            self.screenCapture()
            self.op.append(self.currentContent)
            print(self.op)
            self.opReq = True
            self.operation = "div"

    def per(self):
        try:
            if self.operation!="per":
                self.res(self.operation)

        except:
            print("No operators selected yet.")

        finally:
            self.screenCapture()
            self.op.append(self.currentContent)
            screenContent.set(str(self.currentContent) + "%")
            print(self.op,"%")
            self.opReq = True
            self.operation = "per"

    def res(self, operation): #result
        self.operation = operation
        print(self.operation)

        self.result = 0
        try:
            self.screenCapture()
            self.op.append(self.currentContent)
            print("operands: ", self.op)
            if self.operation == "sum":
                for i in range(len(self.op)):
                    print("operand {}: ".format(i), self.op[i])
                    self.result = self.result + self.op[i]
                    screenContent.set(self.result)

            elif self.operation == "sub":
                self.result = self.op[0] - sum(self.op[1:])
                screenContent.set(self.result)

            elif self.operation == "mul":
                self.result = 1
                for i in self.op:
                    self.result *=i
                screenContent.set(self.result)

            elif self.operation == "div":
                for i in range(len(self.op)):
                    try:
                        aux = self.op[0] / self.op[1]
                        print("AUX: ", aux)
                        self.op.pop(0)
                        self.op.pop(0)
                        self.op.insert(0, aux)
                        print(self.op)
                        self.result = aux
                        screenContent.set(self.result)
                    except:
                        print("finish")

            elif self.operation == "":
                currentContent = screenContent.get()
                if(currentContent.find('.')!=-1) and (currentContent[-1]!="."):
                    self.result=currentContent
                    screenContent.set(self.result)
                elif(currentContent[-1]=="."):
                    self.result=currentContent[:-1]
                    screenContent.set(self.result)
                else:
                    self.result = currentContent
                    screenContent.set(self.result)

            elif self.operation == "per":
                currentContent = int(screenContent.get())
                self.result=currentContent/100
                screenContent.set(self.result)

            print("RESULT: ", self.result)
            self.op = []
            self.opReq = True
            self.operation = ""

        except Exception as e:
            print("err:", e)

    def screenCapture(self):
        self.currentContent = screenContent.get()

        while "%" in self.currentContent:
            self.currentContent = self.currentContent.replace("%", "")
            screenContent.set(self.currentContent)

        if(self.currentContent.find(".")!=-1):
            self.currentContent = float(screenContent.get())
        else:
            self.currentContent = int(screenContent.get())

operations=math_operations()

##--- buttons --- row 2 ---##

button7=Button(mainFrame, text="7", width=3, command=lambda :pushNum("7"))
button7.grid(row=2, column=1)
button8=Button(mainFrame, text="8", width=3, command=lambda :pushNum("8"))
button8.grid(row=2, column=2)
button9=Button(mainFrame, text="9", width=3, command=lambda :pushNum("9"))
button9.grid(row=2, column=3)
buttonDiv=Button(mainFrame, text="/", width=3, command=lambda : operations.div())
buttonDiv.grid(row=2, column=4)

##--- buttons --- row 3 ---##

button4=Button(mainFrame, text="4", width=3, command=lambda :pushNum("4"))
button4.grid(row=3, column=1)
button5=Button(mainFrame, text="5", width=3, command=lambda :pushNum("5"))
button5.grid(row=3, column=2)
button6=Button(mainFrame, text="6", width=3, command=lambda :pushNum("6"))
button6.grid(row=3, column=3)
buttonMul=Button(mainFrame, text="x", width=3, command=lambda :operations.mul())
buttonMul.grid(row=3, column=4)

##--- buttons --- row 4 ---##

button1=Button(mainFrame, text="1", width=3, command=lambda :pushNum("1"))
button1.grid(row=4, column=1)
button2=Button(mainFrame, text="2", width=3, command=lambda :pushNum("2"))
button2.grid(row=4, column=2)
button3=Button(mainFrame, text="3", width=3, command=lambda :pushNum("3"))
button3.grid(row=4, column=3)
buttonSub=Button(mainFrame, text="-", width=3, command=lambda :operations.sub())
buttonSub.grid(row=4, column=4)

##--- buttons --- row 5 ---##

button0=Button(mainFrame, text="0", width=3, command=lambda :pushNum("0"))
button0.grid(row=5, column=1)
buttonPoint=Button(mainFrame, text=".", width=3, command=lambda :pushNum("."))
buttonPoint.grid(row=5, column=2)
buttonPer=Button(mainFrame, text="%", width=3, command=lambda :operations.per())
buttonPer.grid(row=5, column=3)
buttonAdd=Button(mainFrame, text="+", width=3, command=lambda :operations.sum())
buttonAdd.grid(row=5, column=4)

##--- buttons --- row 6 ---##

button0=Button(mainFrame, text="C", width=3, command=lambda :clear())
button0.grid(row=6, column=1)
buttonDel=Button(mainFrame, text="DEL", width=3, command=lambda :delete())
buttonDel.grid(row=6, column=2)
buttonRes=Button(mainFrame, text="=", width=9, command=lambda :operations.res(operations.operation))
buttonRes.grid(row=6, column=3, columnspan=2)
buttonRes.config(bg="#3076ff", fg="#ffffff", relief="raised", state="normal", activeforeground="#ffffff", activebackground="#3681f8")

root.mainloop()